package controllers

import (
	"back_end/middleware"
	"back_end/models"
	"back_end/services"
	"fmt"
	"log"

	"github.com/gofiber/fiber/v2"
)

func EncontrarEmpleados(c *fiber.Ctx) error {
	empleado, err := services.EncontrarEmpleados()
	if err != nil {
		log.Fatal(err)
		return c.Status(fiber.StatusBadGateway).JSON(fiber.Map{
			"success": false,
			"message": "Something went wrong",
			"Erro":    err.Error(),
		})
	}
	return c.Status(fiber.StatusOK).JSON(empleado)
}

func CrearEmpleado(c *fiber.Ctx) error {

	var empleado models.Empleado
	err := c.BodyParser(&empleado)

	if err != nil {
		c.Status(fiber.StatusBadGateway).JSON(fiber.Map{
			"success": false,
			"message": "Cannot parse JSON",
			"error":   err.Error(),
		})
	}

	errors := middleware.ValidarStruct(empleado)
	fmt.Println(empleado)
	if errors != nil {
		return c.Status(fiber.StatusBadRequest).JSON(errors)
	}

	if err = services.ValidacionesBasicasNecesarias(empleado); err != nil {
		return c.Status(fiber.StatusConflict).JSON(err.Error())
	}

	// crear correo valido
	empleado.Correo = middleware.CrearCorreo(empleado)

	_, filt, ID, _ := middleware.IdentityExists(empleado.IDNumero)
	if filt == true {
		return c.JSON(ID)
	}

	empleado, _ = services.CrearEmpleado(empleado)

	fmt.Println("no errors found")

	return c.Status(fiber.StatusOK).JSON(empleado)
}

func EncontrarEmpleado(c *fiber.Ctx) error {
	id := c.Params("id")

	empleado, err := services.EncontrarEmpleado(id)
	if err != nil {
		log.Fatal(err)
		return c.Status(fiber.StatusBadGateway).JSON(fiber.Map{
			"success": false,
			"message": "Cannot parse Id",
			"Error":   err.Error(),
		})
	}
	return c.Status(fiber.StatusOK).JSON(empleado)
}

func ActualizarEmpleado(c *fiber.Ctx) error {
	id := c.Params("id")
	var empleado models.Empleado
	err := c.BodyParser(&empleado)

	if err = services.ValidacionesBasicasNecesarias(empleado); err != nil {
		return c.Status(fiber.StatusConflict).JSON(err.Error())
	}

	if err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(fiber.Map{
			"success": false,
			"message": "Cannot parse JSON",
			"error":   err,
		})
	}
	empleado, _ = services.ActualizarEmpleado(id, empleado)
	return c.Status(fiber.StatusOK).JSON(empleado)

}

func BorrarEmpleado(c *fiber.Ctx) error {
	id := c.Params("id")
	empleado, err := services.BorrarEmpleado(id)
	if err != nil {
		log.Fatal(err)
		return c.Status(fiber.StatusBadGateway).JSON(fiber.Map{
			"success": false,
			"message": "Cannot parse Id",
			"error":   err.Error(),
		})
	}
	return c.Status(fiber.StatusOK).JSON(empleado)
}
