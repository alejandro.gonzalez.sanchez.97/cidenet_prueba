package handlers

import (
	"back_end/router"
	"log"
	"os"

	"github.com/gofiber/fiber/v2/middleware/compress"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

/* Manejadores seteo mi puerto, el Handler y pongo a escuchar al Servidor */
func Manejadores() {

	app := fiber.New()

	app.Use(logger.New())

	// app.Use(cors.New())
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept",
	}))
	/*
		app.Use(cors.New(cors.Options{
			AllowedOrigins:   []string{"http://localhost:8080"},
			AllowCredentials: true,
			Debug:            true, // disable it in production
		}))
	*/
	app.Use(compress.New())

	router.UserRouter(app)

	PORT := os.Getenv("PORT")
	if PORT == "" {
		PORT = "8080"
	}

	log.Fatal(app.Listen(":" + PORT))
	//handler := cors.AllowAll().Handler({}) //(router)
	//log.Fatal(http.ListenAndServe(":"+PORT, handler))
}
