package main

import (
	"back_end/handlers"
	"log"

	"back_end/db"
)

func main() {
	// config db
	if db.ChequeoConnection() == 0 {
		log.Fatal("Sin  conexion a la BD")
		return
	}

	handlers.Manejadores()
}
