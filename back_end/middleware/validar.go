package middleware

import (
	"back_end/db"
	"back_end/models"
	"context"
	"time"

	"github.com/go-playground/validator"
	"go.mongodb.org/mongo-driver/bson"
	valid "gopkg.in/validator.v2"
)

func ValidarStruct(empleado models.Empleado) []*models.ErrorResponse {
	var errors []*models.ErrorResponse
	validate := validator.New()
	err := validate.Struct(empleado)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var element models.ErrorResponse
			element.FailedField = err.StructNamespace()
			element.Tag = err.Tag()
			element.Value = err.Param()
			errors = append(errors, &element)
		}
	}
	return errors
}

/* Solo se validara que los caracteres sean de la a z, tanto en minuscula como en mayuscula */
func ValidacionBasica(paraValidar string, regexp string) error {
	err := valid.Valid(paraValidar, "regexp=^"+regexp+"$")

	return err
}

func IdentityExists(numeroindentidad string) (models.Empleado, bool, string, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := db.MongoCN.Database("CidenetEmpleados") // TODO STORE THIS IN A COMPONENT VARIABLE OR IN .ENV
	col := db.Collection("empleados")

	var empleado models.Empleado

	err := col.FindOne(ctx, bson.M{"idNumero": numeroindentidad}).Decode(&empleado)
	ID := empleado.IDNumero //.Hex() // TODO TEST THIS
	if err != nil {
		return empleado, false, ID, err
	}
	return empleado, true, ID, err

}

/* Se creara un correo valido para el usuario actual */
func CrearCorreo(empleado models.Empleado) string {
	dominio := "cidenet.com"

	if empleado.Pais == "Colombia" {
		dominio = dominio + ".co"
	} else {
		dominio = dominio + ".us"
	}

	email := empleado.PrimerNombre + "." + empleado.PrimerApellido + "." + empleado.IDNumero + "@" + dominio

	return email
}
