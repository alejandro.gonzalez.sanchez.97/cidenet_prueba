package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Empleado struct {
	ID                 primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	IDNumero           string             `json:"idNumero" validate:"required,min=3,max=20"`
	PrimerApellido     string             `json:"primerApellido,omitempty" validate:"required,min=3,max=20"`
	SegundoApellido    string             `json:"segundoApellido,omitempty"`
	PrimerNombre       string             `json:"primerNombre,omitempty" validate:"required,min=3,max=30"`
	OtrosNombres       string             `json:"otrosNombres,omitempty" validate:"min=0,max=50"`
	Pais               string             `json:"pais,omitempty" validate:"required"`
	TipoIdentificacion string             `json:"tipoIdentificacion,omitempty" validate:"required"`
	Area               string             `json:"area,omitempty"`
	//NumeroIdentidad    string             `json:"numeroIdentidad,omitempty" validate:"required,min=3,max=20"`
	Correo             string    `json:"correo,omitempty"`
	Estado             string    `json:"estado,omitempty"`
	FechaRegistro      time.Time `json:"fechaRegistro" bson:"fechaRegistro"`
	FechaIngreso       time.Time `json:"fechaIngreso" bson:"fechaIngreso"`
	FechaActualizacion time.Time `json:"fechaActualizacion" bson:"fechaActualizacion"`
}
