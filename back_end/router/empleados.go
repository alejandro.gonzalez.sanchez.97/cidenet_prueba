package router

import (
	"back_end/controllers"

	"github.com/gofiber/fiber/v2"
)

func UserRouter(app *fiber.App) {
	app.Get("/api/empleado", controllers.EncontrarEmpleados)
	app.Post("/api/empleado", controllers.CrearEmpleado)
	app.Get("/api/empleado/:id", controllers.EncontrarEmpleado)
	app.Patch("/api/empleado/:id", controllers.ActualizarEmpleado)
	app.Delete("/api/empleado/:id", controllers.BorrarEmpleado)
}
