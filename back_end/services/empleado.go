package services

import (
	"back_end/db"
	"back_end/middleware"
	"back_end/models"
	"context"
	packageErrors "errors"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func EncontrarEmpleados() ([]models.Empleado, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	var empleados []models.Empleado

	db := db.MongoCN.Database("CidenetEmpleados") // TODO STORE THIS IN A COMPONENT VARIABLE OR IN .ENV
	col := db.Collection("empleados")

	cursor, err := col.Find(ctx, bson.D{})
	if err = cursor.All(ctx, &empleados); err != nil {
		log.Fatal(err)
		return empleados, err
	}
	return empleados, nil
}

func CrearEmpleado(empleado models.Empleado) (models.Empleado, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := db.MongoCN.Database("CidenetEmpleados") // TODO STORE THIS IN A COMPONENT VARIABLE OR IN .ENV
	col := db.Collection("empleados")

	resultado, err := col.InsertOne(ctx, empleado)
	if err != nil {
		log.Fatal(err)
		return models.Empleado{}, err
	}
	fmt.Println(resultado.InsertedID)
	return empleado, nil
}

func EncontrarEmpleado(empleadoID string) (models.Empleado, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := db.MongoCN.Database("CidenetEmpleados") // TODO STORE THIS IN A COMPONENT VARIABLE OR IN .ENV
	col := db.Collection("empleados")

	var empleado models.Empleado
	objectId, err := primitive.ObjectIDFromHex(empleadoID)
	if err != nil {
		log.Fatal(err)
		return empleado, err
	}

	fmt.Print("about to find worker")
	err = col.FindOne(ctx, bson.M{"_id": objectId}).Decode(&empleado)
	if err != nil {
		log.Fatal(err)
		return empleado, err
	}
	return empleado, nil
}

func ActualizarEmpleado(empleadoID string, empleado models.Empleado) (models.Empleado, error) {
	objectId, err := primitive.ObjectIDFromHex(empleadoID)
	if err != nil {
		log.Fatal(err)
		return empleado, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	db := db.MongoCN.Database("CidenetEmpleados") // TODO STORE THIS IN A COMPONENT VARIABLE OR IN .ENV
	col := db.Collection("empleados")

	filter := bson.M{"_id": objectId}

	update := bson.M{"$set": empleado}
	resultado, err := col.UpdateOne(ctx, filter, update)
	if err != nil {
		log.Fatal(err)
		return models.Empleado{}, err
	}
	fmt.Println(resultado.UpsertedID)
	return empleado, nil
}

func BorrarEmpleado(userId string) (models.Empleado, error) {
	db := db.MongoCN.Database("CidenetEmpleados") // TODO STORE THIS IN A COMPONENT VARIABLE OR IN .ENV
	col := db.Collection("empleados")

	var empleado models.Empleado
	objectId, err := primitive.ObjectIDFromHex(userId)
	if err != nil {
		log.Fatal(err)
		return empleado, err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	resultado, err := col.DeleteOne(ctx, bson.M{"_id": objectId})
	if err != nil {
		log.Fatal(err)
		return empleado, err
	}
	fmt.Println(resultado.DeletedCount)
	return empleado, nil
}

/* Se realizara todas las validaciones necesarias para el ingreso de data nueva a la bd */
func ValidacionesBasicasNecesarias(empleado models.Empleado) error {
	// validar primer nombre
	err := middleware.ValidacionBasica(empleado.PrimerNombre, "[a-zA-Z]*")
	if err != nil {
		err = packageErrors.New("Asegurarse de que primer nombre tengan caracteres de la a la z")
		return err
	}
	// validar primer apellido
	err = middleware.ValidacionBasica(empleado.PrimerApellido, "[a-zA-Z]*")
	if err != nil {
		err = packageErrors.New("Asegurarse de que primer apellido tengan caracteres de la a la z")
		return err
	}
	// validar segundo nombre
	err = middleware.ValidacionBasica(empleado.SegundoApellido, "[a-zA-Z]*")
	if err != nil {
		err = packageErrors.New("Asegurarse de que segundo nombre tengan caracteres de la a la z")
		return err
	}
	// validar otros nombres
	err = middleware.ValidacionBasica(empleado.OtrosNombres, "[a-zA-Z]*")
	if err != nil {
		err = packageErrors.New("Asegurarse de que otros nombres tengan caracteres de la a la z")
		return err
	}

	// validar pais
	// TODO USE ENUMS OR A MAP/ARRAY FOR THIS
	if empleado.Pais != "Colombia" && empleado.Pais != "Estados Unidos" {
		err = packageErrors.New("Pais invalido, solo se permite Colombia y Estados Unidos")
		return err
	}
	// validar tipo de identifiacion
	if empleado.TipoIdentificacion != "Cedula de Ciudadania" && empleado.TipoIdentificacion != "Cedula de Extranjeria" && empleado.TipoIdentificacion != "Pasaporte" && empleado.TipoIdentificacion != "Permiso Especial" {
		err = packageErrors.New("Tipo de validacion invalido, no hace parte de los valores validos")
		return err
	}
	// validar numero de identificacin
	err = middleware.ValidacionBasica(empleado.IDNumero, "[a-zA-Z0-9]*")
	if err != nil {
		err = packageErrors.New("Numero de Id invalido, no hace parte de los valores validos")
		return err
	}
	return nil
}
