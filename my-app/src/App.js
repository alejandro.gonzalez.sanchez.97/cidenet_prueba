import React from "react";
import logo from "./logo.svg";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
//import Empleados from "./Empleados";
//import { conseguirEmpleados } from "./Empleados";
import Empleados from "./Empleados";

import {
  Table,
  Button,
  Container,
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  ModalFooter,
} from "reactstrap"; // importamos todos los componentes que necesitamos de reactstrap


class App extends React.Component {
  render() {

    return (
      <>
        <div>
          <Container>
            <Empleados />
          </Container>
        </div>
      </>
    );
  }
}
export default App;
