import React, { Component } from "react";
import axios from "axios";
import { Card, Header, Form, Input, Icon } from "semantic-ui-react";
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown'

//import { Card, Header, Form, Input, Icon } from "semantic-ui-css/semantic.min.cs";
import {
  Table,
  Button,
  Container,
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  ModalFooter,
} from "reactstrap";

let endpoint = "http://localhost:8080";

export default class Empleados extends React.Component {

  state = {
    // Api states
    empleados: [],
    searchNombre: null,
    searchOtrosNombres: null,
    searchPrimerApellido: null,
    searchSegundoApellido: null,
    searchIDNumero: null,
    currentSearch: null,
    searchPais: null,

    // manejo de modales states
    modalActualizar: false,
    modalInsertar: false,
    form: {
      _id: "",
      IDNumero: "",
      primerNombre: "",
      otrosNombres: "",
      primerApellido: "",
      segundoApellido: "",
      pais: "",
      area: "",
      tipoIdentificacion: ""
    },
  };

  // API

  componentDidMount() {
    this.conseguirEmpleados();
  }

  async conseguirEmpleados() {
    await axios.get(endpoint + `/api/empleado`)
      .then(res => {
        const empleados = res.data;
        this.setState({ empleados });
      }).catch(error => {
        console.log(error)
      })
  }

  editarEmpleado = async (empleado) => {
    console.log(empleado);

    // actualizar lista de forma local
    var contador = 0;
    var arreglo = this.state.empleados;
    arreglo.map((registro) => {
      if (empleado._id == registro._id) {
        arreglo[contador].idNumero = empleado.idNumero;
        arreglo[contador].primerNombre = empleado.primerNombre;
        arreglo[contador].otrosNombres = empleado.otrosNombres;
        arreglo[contador].primerApellido = empleado.primerApellido;
        arreglo[contador].segundoApellido = empleado.segundoApellido;
        arreglo[contador].pais = empleado.pais;
        arreglo[contador].area = empleado.area;
        arreglo[contador].tipoIdentificacion = empleado.tipoIdentificacion;
      }
      contador++;
    });
    this.setState({ empleados: arreglo, modalActualizar: false });

    // actualizar base de datos

    var id = empleado._id;

    await axios
      .patch(endpoint + "/api/empleado/" + id,
      {
        "idNumero":empleado.idNumero,
        "primerNombre":empleado.primerNombre,
        "otrosNombres":empleado.otrosNombres == null ? "" : empleado.otrosNombres,
        "primerApellido":empleado.primerApellido,
        "segundoApellido":empleado.segundoApellido,
        "pais":empleado.pais,
        "area":empleado.area,
        "tipoIdentificacion":empleado.tipoIdentificacion,
        "correo":empleado.correo
      },
      {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res);
        this.conseguirEmpleados();
      });

      // Borrar campo opcional de otros nombres
      this.setState({
        form: {
          ...this.state.form,
          otrosNombres: " ",
        },
      });
  };

  eliminar = (empleado) => {
    var opcion = window.confirm("Estás Seguro que deseas Eliminar el empleado?");
    if (opcion == true) {

      // actualizar lista de forma local

      var contador = 0;
      var arreglo = this.state.empleados;
      arreglo.map((registro) => {
        if (empleado._id == registro._id) {
          arreglo.splice(contador, 1);
        }
        contador++;
      });
      this.setState({ empleados: arreglo, modalActualizar: false });

      console.log("empleados after borrar");
      console.log(this.empleados);

      // actualizar bd

      var id = empleado._id;

      axios.delete(endpoint + `/api/empleado/` + id)
    }
  };

  // Manejo de modales

  handleSelectedTipoIdentificacion = (e) => {
    this.setState({
      form: {
        ...this.state.form,
        tipoIdentificacion: e,
      },
    });
  };

  handleSelectedArea = (e) => {
    this.setState({
      form: {
        ...this.state.form,
        area: e,
      },
    });
  };


  searchNombreSpace=(event)=>{
    let keyword = event.target.value;
    this.setState({searchNombre:keyword});
    this.setState({currentSearch:"primer_nombre"});
  }
  searchOtrosNombresSpace=(event)=>{
    let keyword = event.target.value;
    this.setState({otrosNombres:keyword});
    this.setState({currentSearch:"otros_nombres"});
  }
  searchPrimerApellidoSpace=(event)=>{
    let keyword = event.target.value;
    this.setState({searchPrimerApellido:keyword})
    this.setState({currentSearch:"primer_apellido"});
  }
  searchSegundoApellidoSpace=(event)=>{
    let keyword = event.target.value;
    this.setState({searchSegundoApellido:keyword})
    this.setState({currentSearch:"segundo_apellido"});
  }
  searchIDNumero=(event)=>{
    let keyword = event.target.value;
    this.setState({searchIDNumero:keyword})
    this.setState({currentSearch:"numero_identificacion"});
  }
  searchPais=(event)=>{
    let keyword = event.target.value;
    this.setState({searchPais:keyword})
    this.setState({currentSearch:"pais"});
  }

  mostrarModalInsertar = () => {
    this.setState({
      modalInsertar: true,
    });
  };

  mostrarModalActualizar = (dato) => {
    this.setState({
      form: dato,
      modalActualizar: true,
    });
  };

  cerrarModalActualizar = () => {
    this.setState({ modalActualizar: false });
  };

  cerrarModalInsertar = () => {
    this.setState({ modalInsertar: false });
  };

  insertarEmpleado= async ()=>{
    console.log('insertando empleado');
    var valorNuevo= {...this.state.form};

    // guardar valor localmente
    // valorNuevo.id=this.state.data.length+1;
    var lista= this.state.empleados;
    lista.push(valorNuevo);
    console.log('lista nueva:');
    console.log(lista);
    // this.setState({ modalInsertar: false, empleados: lista });

    // guardar valor en la bd

    if (valorNuevo) {
      await axios
        .post(
          endpoint + "/api/empleado",
          {
            "idNumero":valorNuevo.IDNumero,
            "primerNombre":valorNuevo.primerNombre,
            "otrosNombres":valorNuevo.otrosNombres == null ? "" : valorNuevo.otrosNombres,
            "primerApellido":valorNuevo.primerApellido,
            "segundoApellido":valorNuevo.segundoApellido,
            "area":valorNuevo.area,
            "pais":valorNuevo.pais,
            "tipoIdentificacion":valorNuevo.tipoIdentificacion
          },
          {
            headers: {
              "Content-Type": "application/json"
            }
          }
        )
        .then(res => {
          console.log('fue insertado el nuevo empleado');
          console.log(res.data);
          this.conseguirEmpleados();
          this.setState({
            modalInsertar: false,
            empleados: lista
          });
        }).catch(error => {
          console.error('error: ', error);
        });

        // Borrar campo opcional de otros nombres
        this.setState({
          form: {
            ...this.state.form,
            otrosNombres: " ",
          },
        });
    }
  }

  handleChange = (e) => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };

  render() {
    const styleInfo = {
      paddingRight:'10px',
      width: '4000px'
    }
    const elementStyle ={
      border:'solid',
      borderRadius:'10px',
      position:'relative',
      left:'1vh',
      height:'3vh',
      width:'35vh',
      marginTop:'5vh',
      marginBottom:'10vh'
    }
    const items = this.state.empleados.filter((data)=>{

      switch (this.state.currentSearch) {
        case "primer_nombre":
          console.log('en primer nombre');
          // Busqueda por nombre
          if(this.state.searchNombre == null)
            return data
          else if(data.primerNombre.toLowerCase().includes(this.state.searchNombre.toLowerCase()) || data.primerNombre.toLowerCase().includes(this.state.searchNombre.toLowerCase())){
            return data
          }

          break;
          case "otros_nombres":
            console.log('en segundo nombre');
            // Busqueda por nombre
            if(this.state.searchOtrosNombres == null)
              return data
            else if(data.otrosNombres.toLowerCase().includes(this.state.searchOtrosNombres.toLowerCase()) || data.otrosNombres.toLowerCase().includes(this.state.searchOtrosNombres.toLowerCase())){
              return data
            }

            break;
        case "primer_apellido":
          console.log('en primer apellido');
          // Busqueda por primer apellido
          if(this.state.searchPrimerApellido == null)
            return data;
          else if(data.primerApellido.toLowerCase().includes(this.state.searchPrimerApellido.toLowerCase()) || data.primerApellido.toLowerCase().includes(this.state.searchPrimerApellido.toLowerCase())){
            return data
          }
          break;
          case "segundo_apellido":
            console.log('en segundo apellido');
            // Busqueda por primer apellido
            if(this.state.searchSegundoApellido == null)
              return data;
            else if(data.segundoApellido.toLowerCase().includes(this.state.searchSegundoApellido.toLowerCase()) || data.segundoApellido.toLowerCase().includes(this.state.searchSegundoApellido.toLowerCase())){
              return data
            }
            break;
        case "numero_identificacion":
          if(this.state.searchIDNumero == null)
            return data;
          else if(data.idNumero.toLowerCase().includes(this.state.searchIDNumero.toLowerCase()) || data.idNumero.toLowerCase().includes(this.state.searchIDNumero.toLowerCase())){
            return data
          }

          break;
          case "pais":
            if(this.state.searchPais == null)
              return data;
            else if(data.pais.toLowerCase().includes(this.state.searchPais.toLowerCase()) || data.pais.toLowerCase().includes(this.state.searchPais.toLowerCase())){
              return data
            }

            break;
        default:
          return data;
      }
    }).map(empleado=>{
      return(
      /*
      <div>
        <ul>
          <li style={{position:'relative',left:'10vh'}}>
            <span style={styleInfo}>{data.primerNombre}</span>
            <span style={styleInfo}>{data.primerApellido}</span>
            <span style={styleInfo}>{data.pais}</span>
          </li>
        </ul>
      </div>
      */
      <tbody>
        <tr key={empleado._id}>
          <td>{empleado.idNumero}</td>
          <td>{empleado.primerNombre + ' ' + empleado.otrosNombres + ' ' + empleado.primerApellido + ' ' + empleado.segundoApellido}</td>
          <td>{empleado.pais}</td>
          <td>{empleado.area}</td>
          <td>{empleado.correo}</td>
          <td>
            <Button
              color="primary"
              onClick={() => this.mostrarModalActualizar(empleado)}
            >
              Editar
            </Button>
            <Button color="danger" onClick={()=> this.eliminar(empleado)}>Eliminar</Button>

          </td>
        </tr>
      </tbody>
      )
    })
    const valorDefaultDropdownMenus = "Seleccionar un valor";
    return (
      <>
        <Container>
        <br />
          <Button color="success" onClick={()=> this.mostrarModalInsertar()}>Crear</Button>
          <br />
          <br />
          <Table>
            <thead>
              <tr>
                <th>ID</th>
                <th>Nombre Completo</th>
                <th>Pais</th>
                <th>Area</th>
                <th>Correo</th>
                <th>Acciones</th>
              </tr>
            </thead>

            {items}
          </Table>
          <input type="text" placeholder="Buscar por primer nombre" style={elementStyle} onChange={(e)=>this.searchNombreSpace(e)} />
          <input type="text" placeholder="Buscar por otros nombres" style={elementStyle} onChange={(e)=>this.searchOtrosNombresSpace(e)} />
          <input type="text" placeholder="Buscar por primer apellido" style={elementStyle} onChange={(e)=>this.searchPrimerApellidoSpace(e)} />
          <input type="text" placeholder="Buscar por segundo apellido" style={elementStyle} onChange={(e)=>this.searchSegundoApellidoSpace(e)} />
          <input type="text" placeholder="Buscar por numero de identificacion" style={elementStyle} onChange={(e)=>this.searchIDNumero(e)} />
          <input type="text" placeholder="Buscar por pais" style={elementStyle} onChange={(e)=>this.searchPais(e)} />
        </Container>

        {/*Modal para crear nuevo empleado*/}
        <Modal isOpen={this.state.modalInsertar}>
          <ModalHeader>
           <div><h3>Insertar nuevo empleado</h3></div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>
                Id:
              </label>

              <input
                className="form-control"
                name="IDNumero"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Primer Nombre:
              </label>
              <input
                className="form-control"
                name="primerNombre"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Otros Nombres:
              </label>
              <input
                className="form-control"
                name="otrosNombres"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Primer Apellido:
              </label>
              <input
                className="form-control"
                name="primerApellido"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Segundo Apellido:
              </label>
              <input
                className="form-control"
                name="segundoApellido"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Pais:
              </label>

              <input
                className="form-control"
                name="pais"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Tipo de Identificacion:
              </label>

              <DropdownButton
                alignRight
                title={this.state.form.tipoIdentificacion != null ? this.state.form.tipoIdentificacion : "Seleccionar un valor" }
                id="dropdown-menu-align-right"
                name="tipoIdentificacion"
                onSelect={this.handleSelectedTipoIdentificacion}

                  >
                      <Dropdown.Item eventKey="Cedula de Ciudadania">Cedula de Ciudadania</Dropdown.Item>
                      <Dropdown.Item eventKey="Cedula de Extranjeria">Cedula de Extranjeria</Dropdown.Item>
                      <Dropdown.Item eventKey="Pasaporte">Pasaporte</Dropdown.Item>
                      <Dropdown.Item eventKey="Permiso Especial">Permiso Especial</Dropdown.Item>
              </DropdownButton>
            </FormGroup>

            <FormGroup>
              <label>
                Area:
              </label>

              <DropdownButton
                alignRight
                title={this.state.form.area != null ? this.state.form.area : valorDefaultDropdownMenus }
                id="dropdown-menu-align-right"
                name="tipoIdentificacion"
                onSelect={this.handleSelectedArea}

                  >
                      <Dropdown.Item eventKey="Administración">Administración</Dropdown.Item>
                      <Dropdown.Item eventKey="Financiera">Financiera</Dropdown.Item>
                      <Dropdown.Item eventKey="Compras">Compras</Dropdown.Item>
                      <Dropdown.Item eventKey="Infraestructural">Infraestructura</Dropdown.Item>
                      <Dropdown.Item eventKey="Operación">Operación</Dropdown.Item>
                      <Dropdown.Item eventKey="Talento Humano">Talento Humano</Dropdown.Item>
              </DropdownButton>
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button
              color="primary"
              onClick={() => this.insertarEmpleado()}
            >
              Insertar
            </Button>
            <Button
              className="btn btn-danger"
              onClick={() => this.cerrarModalInsertar()}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>


        {/* Modal solo para actualizar registro actual seleccionado*/}
        <Modal isOpen={this.state.modalActualizar}>
          <ModalHeader>
           <div><h3>Editar Empleado</h3></div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>
                Id:
              </label>

              <input
                className="form-control"
                name="IDNumero"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.idNumero}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Primer Nombre:
              </label>
              <input
                className="form-control"
                name="primerNombre"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.primerNombre}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Otros Nombres:
              </label>
              <input
                className="form-control"
                name="otrosNombres"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.otrosNombres}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Primer Apellido:
              </label>
              <input
                className="form-control"
                name="primerApellido"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.primerApellido}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Primer Apellido:
              </label>
              <input
                className="form-control"
                name="segundoApellido"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.segundoApellido}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Pais:
              </label>
              <input
                className="form-control"
                name="pais"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.pais}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Tipo de Identificacion:
              </label>
              <input
                className="form-control"
                name="tipoIdentificacion"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.tipoIdentificacion}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Area:
              </label>

              <DropdownButton
                alignRight
                title={this.state.form.area != null ? this.state.form.area : valorDefaultDropdownMenus }
                id="dropdown-menu-align-right"
                name="tipoIdentificacion"
                onSelect={this.handleSelectedArea}

                  >
                      <Dropdown.Item eventKey="Administración">Administración</Dropdown.Item>
                      <Dropdown.Item eventKey="Financiera">Financiera</Dropdown.Item>
                      <Dropdown.Item eventKey="Compras">Compras</Dropdown.Item>
                      <Dropdown.Item eventKey="Infraestructural">Infraestructura</Dropdown.Item>
                      <Dropdown.Item eventKey="Operación">Operación</Dropdown.Item>
                      <Dropdown.Item eventKey="Talento Humano">Talento Humano</Dropdown.Item>
              </DropdownButton>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button
              color="primary"
              onClick={() => this.editarEmpleado(this.state.form)}
            >
              Editar
            </Button>
            <Button
              color="danger"
              onClick={() => this.cerrarModalActualizar()}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }

  // original render just for showing data
  /*
  render() {
    return (
      <ul>
        {this.state.empleados.map(empleado => <div><li>{empleado.primerNombre}</li><li>{empleado.primerApellido}</li><br></br></div>)}
      </ul>
    )
  }
  */
}
/*
class Empleados {

  async conseguirEmpleados() {
    var empleados = [];

    console.log('retrieving information of empleados');
    const res = await axios.get(endpoint + "/api/empleado")
      .then(response => empleados.push = response.data.data );

    console.log('res:');
    console.log(res);
    console.log('content');
    console.log(empleados)

    return empleados;

    //return res;
  }

  test() {
    return "test";
  }

}
*/


//export default Empleados;
//export const {test, conseguirEmpleados} = new Empleados();
