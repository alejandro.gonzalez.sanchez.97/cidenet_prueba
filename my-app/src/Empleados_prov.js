import React, { Component } from "react";
import axios from "axios";
import { Card, Header, Form, Input, Icon } from "semantic-ui-react";

import Information from './info-json';
//import { Card, Header, Form, Input, Icon } from "semantic-ui-css/semantic.min.cs";
import {
  Table,
  Button,
  Container,
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  ModalFooter,
} from "reactstrap";

let endpoint = "http://localhost:8080";

export default class Empleados extends React.Component {

  state = {
    // Api states
    empleados: [],
    searchNombre: null,
    searchPrimerApellido: null,
    searchIDNumero: null,
    currentSearch: null,
    searchPais: null,

    // manejo de modales states
    modalActualizar: false,
    modalInsertar: false,
    form: {
      _id: "",
      IDNumero: "",
      primerNombre: "",
      primerApellido: "",
      pais: "",
      tipoIdentificacion: ""
    },
  };

  // API

  componentDidMount() {
    this.conseguirEmpleados();
  }

  async conseguirEmpleados() {
    await axios.get(endpoint + `/api/empleado`)
      .then(res => {
        const empleados = res.data;
        this.setState({ empleados });
      }).catch(error => {
        console.log(error)
      })
  }

  editarEmpleado = async (empleado) => {
    console.log(empleado);

    // actualizar lista de forma local
    var contador = 0;
    var arreglo = this.state.empleados;
    arreglo.map((registro) => {
      if (empleado._id == registro._id) {
        arreglo[contador].idNumero = empleado.idNumero;
        arreglo[contador].primerNombre = empleado.primerNombre;
        arreglo[contador].primerApellido = empleado.primerApellido;
        arreglo[contador].pais = empleado.pais;
        arreglo[contador].tipoIdentificacion = empleado.tipoIdentificacion;
      }
      contador++;
    });
    this.setState({ empleados: arreglo, modalActualizar: false });
    console.log("arreglo:")
    console.log(arreglo)

    // actualizar base de datos

    var id = empleado._id;

    await axios
      .patch(endpoint + "/api/empleado/" + id,
      {
        "idNumero":empleado.idNumero,
        "primerNombre":empleado.primerNombre,
        "primerApellido":empleado.primerApellido,
        "pais":empleado.pais,
        "tipoIdentificacion":empleado.tipoIdentificacion,
        "correo":empleado.correo
      },
      {
        headers: {
          "Content-Type": "application/json"
        }
      })
      .then(res => {
        console.log(res);
        this.conseguirEmpleados();
      });
  };

  // Manejo de modales

  searchNombreSpace=(event)=>{
    let keyword = event.target.value;
    this.setState({searchNombre:keyword});
    this.setState({currentSearch:"primer_nombre"});
  }
  searchPrimerApellidoSpace=(event)=>{
    let keyword = event.target.value;
    this.setState({searchPrimerApellido:keyword})
    this.setState({currentSearch:"primer_apellido"});
  }
  searchIDNumero=(event)=>{
    let keyword = event.target.value;
    this.setState({searchIDNumero:keyword})
    this.setState({currentSearch:"numero_identificacion"});
  }
  searchPais=(event)=>{
    let keyword = event.target.value;
    this.setState({searchPais:keyword})
    this.setState({currentSearch:"pais"});
  }

  mostrarModalInsertar = () => {
    this.setState({
      modalInsertar: true,
    });
  };

  mostrarModalActualizar = (dato) => {
    this.setState({
      form: dato,
      modalActualizar: true,
    });
  };

  cerrarModalActualizar = () => {
    this.setState({ modalActualizar: false });
  };

  cerrarModalInsertar = () => {
    this.setState({ modalInsertar: false });
  };

  insertarEmpleado= async ()=>{
    console.log('insertando empleado');
    var valorNuevo= {...this.state.form};

    // guardar valor localmente
    // valorNuevo.id=this.state.data.length+1;
    var lista= this.state.empleados;
    lista.push(valorNuevo);
    console.log('lista nueva:');
    console.log(lista);
    // this.setState({ modalInsertar: false, empleados: lista });

    // guardar valor en la bd

    if (valorNuevo) {
      await axios
        .post(
          endpoint + "/api/empleado",
          {
            "idNumero":valorNuevo.IDNumero,
            "primerNombre":valorNuevo.primerNombre,
            "primerApellido":valorNuevo.primerApellido,
            "pais":valorNuevo.pais,
            "tipoIdentificacion":valorNuevo.tipoIdentificacion
          },
          {
            headers: {
              "Content-Type": "application/json"
            }
          }
        )
        .then(res => {
          console.log('fue insertado el nuevo empleado');
          console.log(res.data);
          this.conseguirEmpleados();
          this.setState({
            modalInsertar: false,
            empleados: lista
          });
        }).catch(error => {
          console.error('error: ', error);
        });
    }
  }

  handleChange = (e) => {
    this.setState({
      form: {
        ...this.state.form,
        [e.target.name]: e.target.value,
      },
    });
  };

  render() {
    const styleInfo = {
      paddingRight:'10px',
      width: '4000px'
    }
    const elementStyle ={
      border:'solid',
      borderRadius:'10px',
      position:'relative',
      left:'1vh',
      height:'3vh',
      width:'35vh',
      marginTop:'5vh',
      marginBottom:'10vh'
    }
    const items = this.state.empleados.filter((data)=>{

      switch (this.state.currentSearch) {
        case "primer_nombre":
          console.log('en primer nombre');
          // Busqueda por nombre
          if(this.state.searchNombre == null)
            return data
          else if(data.primerNombre.toLowerCase().includes(this.state.searchNombre.toLowerCase()) || data.primerNombre.toLowerCase().includes(this.state.searchNombre.toLowerCase())){
            return data
          }

          break;
        case "primer_apellido":
          console.log('en primer apellido');
          // Busqueda por primer apellido
          if(this.state.searchPrimerApellido == null)
            return data;
          else if(data.primerApellido.toLowerCase().includes(this.state.searchPrimerApellido.toLowerCase()) || data.primerApellido.toLowerCase().includes(this.state.searchPrimerApellido.toLowerCase())){
            return data
          }
          break;
        case "numero_identificacion":
          if(this.state.searchIDNumero == null)
            return data;
          else if(data.idNumero.toLowerCase().includes(this.state.searchIDNumero.toLowerCase()) || data.idNumero.toLowerCase().includes(this.state.searchIDNumero.toLowerCase())){
            return data
          }

          break;
          case "pais":
            if(this.state.searchPais == null)
              return data;
            else if(data.pais.toLowerCase().includes(this.state.searchPais.toLowerCase()) || data.pais.toLowerCase().includes(this.state.searchPais.toLowerCase())){
              return data
            }

            break;
        default:
          return data;
      }
    }).map(empleado=>{
      return(
      /*
      <div>
        <ul>
          <li style={{position:'relative',left:'10vh'}}>
            <span style={styleInfo}>{data.primerNombre}</span>
            <span style={styleInfo}>{data.primerApellido}</span>
            <span style={styleInfo}>{data.pais}</span>
          </li>
        </ul>
      </div>
      */
      <tbody>
        <tr key={empleado._id}>
          <td>{empleado.idNumero}</td>
          <td>{empleado.primerNombre + ' ' + empleado.primerApellido}</td>
          <td>{empleado.pais}</td>
          <td>{empleado.correo}</td>
          <td>
            <Button
              color="primary"
              onClick={() => this.mostrarModalActualizar(empleado)}
            >
              Editar
            </Button>
          </td>
        </tr>
      </tbody>
      )
    })

    return (
      <>
        <Container>
        <br />
          <Button color="success" onClick={()=> this.mostrarModalInsertar()}>Crear</Button>
          <br />
          <br />
          <Table>
            <thead>
              <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Pais</th>
                <th>correo</th>
                <th>acciones</th>
              </tr>
            </thead>

            {items}
          </Table>
          <input type="text" placeholder="Buscar por primer nombre" style={elementStyle} onChange={(e)=>this.searchNombreSpace(e)} />
          <input type="text" placeholder="Buscar por primer apellido" style={elementStyle} onChange={(e)=>this.searchPrimerApellidoSpace(e)} />
          <input type="text" placeholder="Buscar por numero de identificacion" style={elementStyle} onChange={(e)=>this.searchIDNumero(e)} />
          <input type="text" placeholder="Buscar por pais" style={elementStyle} onChange={(e)=>this.searchPais(e)} />
        </Container>

        {/*Modal para crear nuevo empleado*/}
        <Modal isOpen={this.state.modalInsertar}>
          <ModalHeader>
           <div><h3>Insertar nuevo empleado</h3></div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>
                Id:
              </label>

              <input
                className="form-control"
                name="IDNumero"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Primer Nombre:
              </label>
              <input
                className="form-control"
                name="primerNombre"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Primer Apellido:
              </label>
              <input
                className="form-control"
                name="primerApellido"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Pais:
              </label>
              <input
                className="form-control"
                name="pais"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Tipo de Identificacion:
              </label>
              <input
                className="form-control"
                name="tipoIdentificacion"
                type="text"
                onChange={this.handleChange}
              />
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button
              color="primary"
              onClick={() => this.insertarEmpleado()}
            >
              Insertar
            </Button>
            <Button
              className="btn btn-danger"
              onClick={() => this.cerrarModalInsertar()}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>


        {/* Modal solo para actualizar registro actual seleccionado*/}
        <Modal isOpen={this.state.modalActualizar}>
          <ModalHeader>
           <div><h3>Editar Empleado</h3></div>
          </ModalHeader>

          <ModalBody>
            <FormGroup>
              <label>
                Id:
              </label>

              <input
                className="form-control"
                name="IDNumero"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.idNumero}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Primer Nombre:
              </label>
              <input
                className="form-control"
                name="primerNombre"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.primerNombre}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Primer Apellido:
              </label>
              <input
                className="form-control"
                name="primerApellido"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.primerApellido}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Pais:
              </label>
              <input
                className="form-control"
                name="pais"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.pais}
              />
            </FormGroup>

            <FormGroup>
              <label>
                Tipo de Identificacion:
              </label>
              <input
                className="form-control"
                name="tipoIdentificacion"
                type="text"
                onChange={this.handleChange}
                value={this.state.form.tipoIdentificacion}
              />
            </FormGroup>
          </ModalBody>

          <ModalFooter>
            <Button
              color="primary"
              onClick={() => this.editarEmpleado(this.state.form)}
            >
              Editar
            </Button>
            <Button
              color="danger"
              onClick={() => this.cerrarModalActualizar()}
            >
              Cancelar
            </Button>
          </ModalFooter>
        </Modal>
      </>
    );
  }

  // original render just for showing data
  /*
  render() {
    return (
      <ul>
        {this.state.empleados.map(empleado => <div><li>{empleado.primerNombre}</li><li>{empleado.primerApellido}</li><br></br></div>)}
      </ul>
    )
  }
  */
}
/*
class Empleados {

  async conseguirEmpleados() {
    var empleados = [];

    console.log('retrieving information of empleados');
    const res = await axios.get(endpoint + "/api/empleado")
      .then(response => empleados.push = response.data.data );

    console.log('res:');
    console.log(res);
    console.log('content');
    console.log(empleados)

    return empleados;

    //return res;
  }

  decirHola() {
    return "hola";
  }

}
*/
class ToDoList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      task: "",
      items: []
    };
  }

  componentDidMount() {
    this.getTask();
  }

  onChange = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  onSubmit = () => {
    let { task } = this.state;
    // console.log("pRINTING task", this.state.task);
    if (task) {
      axios
        .post(
          endpoint + "/api/task",
          {
            task
          },
          {
            headers: {
              "Content-Type": "application/x-www-form-urlencoded"
            }
          }
        )
        .then(res => {
          this.getTask();
          this.setState({
            task: ""
          });
          console.log(res);
        });
    }
  };

  getTask = () => {
    axios.get(endpoint + "/api/task").then(res => {
      console.log(res);
      if (res.data) {
        this.setState({
          items: res.data.map(item => {
            let color = "yellow";

            if (item.status) {
              color = "green";
            }
            return (
              <Card key={item._id} color={color} fluid>
                <Card.Content>
                  <Card.Header textAlign="left">
                    <div style={{ wordWrap: "break-word" }}>{item.task}</div>
                  </Card.Header>

                  <Card.Meta textAlign="right">
                    <Icon
                      name="check circle"
                      color="green"
                      onClick={() => this.updateTask(item._id)}
                    />
                    <span style={{ paddingRight: 10 }}>Done</span>
                    <Icon
                      name="undo"
                      color="yellow"
                      onClick={() => this.undoTask(item._id)}
                    />
                    <span style={{ paddingRight: 10 }}>Undo</span>
                    <Icon
                      name="delete"
                      color="red"
                      onClick={() => this.deleteTask(item._id)}
                    />
                    <span style={{ paddingRight: 10 }}>Delete</span>
                  </Card.Meta>
                </Card.Content>
              </Card>
            );
          })
        });
      } else {
        this.setState({
          items: []
        });
      }
    });
  };

  updateTask = id => {
    axios
      .put(endpoint + "/api/task/" + id, {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      })
      .then(res => {
        console.log(res);
        this.getTask();
      });
  };

  undoTask = id => {
    axios
      .put(endpoint + "/api/undoTask/" + id, {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      })
      .then(res => {
        console.log(res);
        this.getTask();
      });
  };

  deleteTask = id => {
    axios
      .delete(endpoint + "/api/deleteTask/" + id, {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        }
      })
      .then(res => {
        console.log(res);
        this.getTask();
      });
  };
  render() {
    return (
      <div>
        <div className="row">
          <Header className="header" as="h2">
            TO DO LIST
          </Header>
        </div>
        <div className="row">
          <Form onSubmit={this.onSubmit}>
            <Input
              type="text"
              name="task"
              onChange={this.onChange}
              value={this.state.task}
              fluid
              placeholder="Create Task"
            />
            {/* <Button >Create Task</Button> */}
          </Form>
        </div>
        <div className="row">
          <Card.Group>{this.state.items}</Card.Group>
        </div>
      </div>
    );
  }
}

//export default Empleados;
//export const {decirHola, conseguirEmpleados} = new Empleados();
